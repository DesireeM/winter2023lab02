public class Calculator
{
	public static double add (double n1, double n2)
	{
		double n3 = n1 + n2;
		return n3;
	}
	
	public static double substract (double n1, double n2)
	{
		double n3 = n1 - n2;
		return n3;
	}
	
	public double multiply (double n1, double n2)
	{
		double n3 = n1 * n2;
		return n3;
	}
	
	public double divide(double n1, double n2)
	{
		double n3 = n1/n2;
		return n3;
	}
}