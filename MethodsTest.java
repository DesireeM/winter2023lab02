public class MethodsTest
{
	public static void main (String[] args)
	{
		int x = 5;
		System.out.println(x);
		methodNoInputNoReturn();
		System.out.println(x);
		
		
		System.out.println(x);
		methodOneInputNoReturn(x+10);
		System.out.println(x);
		
		int num1 = 1;
		double num2 = 2.5;
		methodTwoInputNoReturn(num1, num2);
		
		int z = methodNoInputReturnInt();
		System.out.println(z);
		
		double squareNum = (sumSquareRoot (9, 5));
		System.out.println(squareNum);
		
		String s1 = "java";
		String s2 = "programming";
		System.out.println(s1.length());
		System.out.println(s2.length());
		
		SecondClass sc = new SecondClass();
		
		System.out.println(SecondClass.addOne(50));
		System.out.println(sc.addTwo(50));
	}

	public static void methodNoInputNoReturn()
	{
		System.out.println("I’m in a method that takes no input and returns nothing");
		int x = 20;
	}
	
	
	public static void methodOneInputNoReturn(int userInput)
	{
		userInput = userInput - 5;
		System.out.println("Inside the method one input no return");
		System.out.println(userInput);
	}
	
	public static void methodTwoInputNoReturn(int num1, double num2)
	{
		System.out.println(num1);
		System.out.println(num2);
	}
	
	public static int methodNoInputReturnInt()
	{
		return 5;
	}
	
	public static double sumSquareRoot (int n1, int n2)
	{
		double n3 = n1 + n2;
		n3 = Math.sqrt(n3);
		return n3;
	}

}
