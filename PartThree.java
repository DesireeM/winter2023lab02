import java.util.Scanner;
public class PartThree
{
	public static void main (String [] args)
	{
		Scanner scan = new Scanner(System.in);
		
		System.out.println("Enter a number to be the first number: ");
		double n1 = scan.nextDouble();
		
		System.out.println("Enter a number to be the second number: ");
		double n2 = scan.nextDouble();
		
		
		System.out.println(Calculator.add(n1, n2));
		System.out.println(Calculator.substract(n1, n2));
		
		Calculator calc = new Calculator();
		System.out.println(calc.multiply(n1, n2));
		System.out.println(calc.divide(n1, n2));
	}
}